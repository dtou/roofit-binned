#ifndef MINIMIZER_CPP
#define MINIMIZER_CPP

#include "Minimizer.h"
#include "RooMinimizer.h"

void Minimizer::minimize(RooAbsReal * loglikelihood){
    RooMinimizer minimizer(*loglikelihood);

    minimizer.setOffsetting(true);
    minimizer.optimizeConst(true);
    minimizer.setStrategy(2);
    minimizer.migrad();
    m_lastResult = minimizer.save();
    if (m_lastResult->status() != 0){
        DeleteResultIfNotNull();
        minimizer.setStrategy(1);
        minimizer.migrad();
        minimizer.setStrategy(2);
        minimizer.migrad();
    }
    DeleteResultIfNotNull();
    minimizer.hesse();
    m_lastResult = minimizer.save();
}

RooFitResult * Minimizer::GetRooFitResult(){
    return m_lastResult;
}

void Minimizer::DeleteResultIfNotNull(){
    if (m_lastResult != nullptr){
        delete m_lastResult;
        m_lastResult = nullptr;
    }
}

#endif
