#ifndef GENERATOR_CPP
#define GENERATOR_CPP

#include "Generator.h"
#include "RooRandom.h"

Generator::Generator(){
    m_Nsignal = 0;
    m_Nbackground = 0;
    m_signal = nullptr;
    m_background = nullptr;
    m_observable = nullptr;

    m_mean  = new RooRealVar("generator_mean", "generator_mean", 0);
    m_sigma = new RooRealVar("generator_sigma", "generator_sigma", 0);
    m_aLow  = new RooRealVar("generator_aLow", "generator_aLow", 0);
    m_nLow  = new RooRealVar("generator_nLow", "generator_nLow", 0);
    m_aHigh = new RooRealVar("generator_aHigh", "generator_aHigh", 0);
    m_nHigh = new RooRealVar("generator_nHigh", "generator_nHigh", 0);
    m_slope = new RooRealVar("generator_slope", "generator_slope", 0);
}

Generator::Generator(RooRealVar& observable) : Generator() {
    SetObservable(observable);
}

Generator::Generator(unsigned long int seed) : Generator() {
    SetSeed(seed);
}

Generator::Generator(RooRealVar& observable, unsigned long int seed) : Generator() {
    SetObservable(observable);
    SetSeed(seed);
}

void Generator::SetObservable(RooRealVar& observable){
    m_observable = &observable;
    DeletePDFIfNotNull();
    m_signal = new RooDSCBShape("generator_signal", "generator_signal",
                                         observable, *m_mean, *m_sigma, 
                                         *m_aLow, *m_nLow, *m_aHigh, *m_nHigh);
    m_background = new RooExponential("generator_background", "generator_background",
                                      observable, *m_slope);
}

void Generator::DeletePDFIfNotNull(){
    if (m_signal != nullptr){
        delete m_signal;
        m_signal = nullptr;
    }
    if (m_background != nullptr){
        delete m_background;
        m_background = nullptr;
    }
}

void Generator::SetSeed(unsigned long int seed){
    m_poissonGenerator.SetSeed(seed);
    RooRandom::randomGenerator()->SetSeed(seed);
}

RooDataSet * Generator::Generate(){
    if(m_observable){
        return GenerateDataSet();
    }
    else{
        return nullptr;
    }
}

RooDataSet * Generator::GenerateDataSet(){
    int n_signal_generate = m_poissonGenerator.Poisson(m_Nsignal);
    int n_background_generate = m_poissonGenerator.Poisson(m_Nbackground);
    RooArgSet observableSet(*m_observable);
    auto signal = m_signal->generate(observableSet, n_signal_generate);
    auto background = m_background->generate(observableSet, n_background_generate);
    signal->append(*background);
    delete background;
    return signal;
}

void Generator::ConfigureSignal(int n_signal, double mean, double sigma, 
                                double aLow, double nLow, double aHigh, double nHigh){
    m_Nsignal = n_signal;
    m_mean->setVal(mean);
    m_sigma->setVal(sigma);
    m_aLow->setVal(aLow);
    m_nLow->setVal(nLow);
    m_aHigh->setVal(aHigh);
    m_nHigh->setVal(nHigh);
}

void Generator::ConfigureBackground(int n_background, double slope){
    m_Nbackground = n_background;
    m_slope->setVal(slope);
}

RooDSCBShape * Generator::GetSignal(){
    return m_signal;
}

RooExponential * Generator::GetBackground(){
    return m_background;
}

Generator::~Generator(){
    DeletePDFIfNotNull();
    delete m_mean;
    delete m_sigma;
    delete m_aLow;
    delete m_nLow;
    delete m_aHigh;
    delete m_nHigh;
    delete m_slope;
}

#endif
