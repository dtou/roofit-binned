#ifndef MINIMIZER_H
#define MINIMIZER_H

#include "RooFitResult.h"
#include "RooAbsReal.h"

class Minimizer{
public:
    Minimizer() = default;
    void minimize(RooAbsReal * loglikelihood);
    RooFitResult * GetRooFitResult();
    void DeleteResultIfNotNull();

private:
    RooFitResult * m_lastResult = nullptr;
};

#endif
