#ifndef GENERATOR_H
#define GENERATOR_H

#include "TRandom3.h"
#include "RooDSCBShape.h"
#include "RooExponential.h"
#include "RooRealVar.h"
#include "RooDataSet.h"

class Generator{ 
    public :
        Generator();
        Generator(RooRealVar& observable);
        Generator(unsigned long int seed);
        Generator(RooRealVar& observable, unsigned long int seed);
        void SetObservable(RooRealVar& observable);
        void SetSeed(unsigned long int seed);
        RooDataSet * Generate();
        void ConfigureSignal(int n_signal, double mean, double sigma,
                             double aLow, double nLow, double aHigh, double nHigh);
        void ConfigureBackground(int n_background, double slope);
        RooDSCBShape * GetSignal();
        RooExponential * GetBackground();
        ~Generator();

    private:
        void DeletePDFIfNotNull();
        RooDataSet * GenerateDataSet();
        TRandom3 m_poissonGenerator;
        int m_Nsignal;
        int m_Nbackground;

        RooRealVar * m_observable;
        RooRealVar * m_aLow;
        RooRealVar * m_nLow;
        RooRealVar * m_mean;
        RooRealVar * m_sigma;
        RooRealVar * m_aHigh;
        RooRealVar * m_nHigh;
        RooRealVar * m_slope;

        RooDSCBShape * m_signal;
        RooExponential * m_background;
};

#endif
