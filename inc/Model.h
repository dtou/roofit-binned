#ifndef MODEL_H
#define MODEL_H

#include "RooRealVar.h"
#include "RooAddPdf.h"
#include "RooDSCBShape.h"
#include "RooExponential.h"

class Model{
public:
    Model();
    Model(RooRealVar& observable);
    void SetObservable(RooRealVar& observable);
    void ConfigureSignal(int n_signal, double mean, double sigma,
                         double aLow, double nLow, double aHigh, double nHigh);
    void ConfigureBackground(int n_background, double slope);
    RooAbsPdf * GetPDF();
    RooDSCBShape * GetSignal();
    RooExponential * GetBackground();
    ~Model();

private:
    void DeletePDFIfNotNull();

private:
    RooRealVar * m_Nsignal;
    RooRealVar * m_Nbackground;
    RooRealVar * m_mean;
    RooRealVar * m_sigma;
    RooRealVar * m_aLow;
    RooRealVar * m_nLow;
    RooRealVar * m_aHigh;
    RooRealVar * m_nHigh;
    RooRealVar * m_slope;

    RooDSCBShape * m_signal;
    RooExponential * m_background;
    RooAddPdf * m_pdf;

};

#endif
