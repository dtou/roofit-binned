#ifndef AGGREGATE_CPP
#define AGGREGATE_CPP

#include "cxxopts.hpp"
#include "TLine.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TAxis.h"

#include <tuple>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>
#include <regex>
#include <algorithm>
#include <numeric>

struct Result{
    double mean;
    double mean_error;
    double width;
    double width_error;
};

struct Pulls{
    Result mu;
    Result nbkg;
    Result nsig;
    Result sigma;
    Result slope;
};

std::map<int, std::string> GetPullLogs(std::string listfile){
    std::map <int, std::string> pullLogs;

    const std::regex whitespace("\\s+");
    std::ifstream file(listfile.c_str());
    std::string line;
    std::sregex_token_iterator iter_end;
    while (std::getline(file, line)){
        std::regex_token_iterator iter(line.begin(), line.end(), whitespace, -1);
        int key = std::stoi(iter->str());
        pullLogs[key] = (std::next(iter)->str());
    }
    file.close();
    return pullLogs;
}



Pulls GetPullsFromLog(std::string logfile){
    const std::regex pattern("(^mu|^nbkg|^nsig|^sigma|^slope).*");
    const std::regex whitespace("\\s+");

    std::ifstream file(logfile.c_str());
    std::string line;

    std::map<std::string, Result> resultMap;
    while (std::getline(file, line)){
        if (std::regex_match(line, pattern)){
            std::vector <std::string> entries;
            std::sregex_token_iterator iter(line.begin(), line.end(), whitespace, -1);
            std::sregex_token_iterator end;
            for (; iter!=end; iter++){
                entries.push_back(*iter);
            }
            auto key = entries[0];
            resultMap[key] = {
                std::stod(entries[1]),
                std::stod(entries[3]),
                std::stod(entries[4]),
                std::stod(entries[6])};
        }
    }
    file.close();

    Pulls pull = {
        resultMap["mu"],
        resultMap["nbkg"],
        resultMap["nsig"],
        resultMap["sigma"],
        resultMap["slope"]};
    return std::move(pull);
}

std::map<int, Pulls> GetPulls(std::map<int, std::string> pullLogs){
    std::map<int, Pulls> pulls;
    for (const auto& pair : pullLogs){
        pulls[pair.first] = GetPullsFromLog(pair.second);
    }
    return std::move(pulls);
}

void DrawCompatibilityPlot(const std::string outputdir, const std::string & title, const std::string & yTitle, 
                           double expectedValue, double defaultRange, 
                           const std::vector <double> & values, const std::vector<double> & errors, 
                           const std::vector<double> & y_values, const std::string & extension,
                           bool drawLegend = false){
    size_t n = values.size();
    double y_spacing = y_values[1] - y_values[0];
    std::vector<double> small_width(n, 0.14 * y_spacing);
    std::vector<double> y_width(n, 0.15 * y_spacing);
    std::vector<double> errors_3sigma(errors.begin(), errors.end());
    std::for_each(errors_3sigma.begin(), errors_3sigma.end(), [](double& x) { x*=3; });
    std::vector<double> absoluteDeviation(values.begin(), values.end());
    std::for_each(absoluteDeviation.begin(), absoluteDeviation.end(), [expectedValue](double& x) { x = abs(x - expectedValue); });

    auto valueRange = std::max(0.5, *std::max_element(absoluteDeviation.begin(), absoluteDeviation.end()) * 1.2);
    TCanvas canvas("canvas", "canvas", 800, 600);
    canvas.SetLeftMargin(0.15);
    canvas.SetRightMargin(0.05);
    canvas.SetBottomMargin(0.18);
    canvas.SetTopMargin(0.10);
    canvas.RangeAxis(expectedValue - valueRange, 0, expectedValue + valueRange, n);
    TGraphErrors graph_1sigma(n, &values[0], &y_values[0], &errors[0], &y_width[0]);
    TGraphErrors graph_3sigma(n, &values[0], &y_values[0], &errors_3sigma[0], &small_width[0]);

    graph_3sigma.GetXaxis()->Set(100, expectedValue - valueRange, expectedValue + valueRange);
    graph_3sigma.SetMaximum(y_values.back() + 0.5 * y_spacing);
    graph_3sigma.SetMinimum(y_values[0] - 0.5 * y_spacing);

    graph_3sigma.SetTitle(" ");
    graph_3sigma.SetLineWidth(1);
    graph_3sigma.GetXaxis()->SetTitle(title.c_str());
    graph_3sigma.GetXaxis()->CenterTitle(true);
    graph_3sigma.GetXaxis()->SetTitleSize(0.08);
    graph_3sigma.GetXaxis()->SetLabelSize(0.08);
    graph_3sigma.GetXaxis()->SetNdivisions(4, 5, 0);
    graph_3sigma.GetYaxis()->SetTitle(yTitle.c_str());
    graph_3sigma.GetYaxis()->CenterTitle(true);
    graph_3sigma.GetYaxis()->SetTitleSize(0.08);
    graph_3sigma.GetYaxis()->SetLabelSize(0.08);
    graph_3sigma.GetYaxis()->SetTitleOffset(0.9);
    graph_3sigma.GetYaxis()->SetMaxDigits(3);
    graph_3sigma.GetYaxis()->SetNdivisions(6, 5, 0);
    graph_3sigma.Draw("AP");
    graph_1sigma.SetFillStyle(1001);
    graph_1sigma.SetFillColor(870);
    graph_1sigma.SetMarkerStyle(8);
    graph_1sigma.SetMarkerSize(1);
    graph_1sigma.Draw("2P");
    TLine line(expectedValue, graph_3sigma.GetMinimum(), expectedValue, graph_3sigma.GetMaximum());
    line.SetLineColorAlpha(921, 0.5);
    line.Draw("same");

    std::string outputname = outputdir + "/" + title + "." + extension;
    canvas.SaveAs(outputname.c_str());

    if(drawLegend) {
        TCanvas cLegend("cLegend", "cLegend", 800, 150);
        cLegend.cd();
        TLegend legend(.005, .005, .995, .995);
        legend.AddEntry(&graph_1sigma, "1 #sigma bound", "F");
        legend.AddEntry(&graph_3sigma, "3 #sigma bound", "LEP");
        legend.SetNColumns(2);
        legend.SetBorderSize(0);
        legend.Draw();
        std::string legendFilepath = outputdir + "/legends.pdf";
        cLegend.SaveAs(legendFilepath.c_str());
    }
}

int main(int argc, char * argv[]){
    cxxopts::Options options("truth", "Signal: double-sided crystall ball (DSCB); Background : exponential");

    options.add_options("Input")
        ("l,list", "List of path to pull logs", cxxopts::value<std::string>()->default_value("pulls.list"));

    options.add_options("Outputs")
        ("o,outputdir", "The directory to dump the plots.", cxxopts::value<std::string>()->default_value("."))
        ("e,extension", "The output extension to plot the pulls. 'pdf'(default) or 'png'", cxxopts::value<std::string>()->default_value("pdf"))
        ("y,ytitle", "The pull trend variable title", cxxopts::value<std::string>()->default_value("#sigma"));

    options.add_options()
        ("h,help", "Print usage");

    auto args = options.parse(argc, argv);

    if (args.count("help")){
      std::cout << options.help() << std::endl;
      exit(0);
    }

    std::string extension = args["extension"].as<std::string>();
    if ((extension != "png") && (extension != "pdf")){
        std::cout << "--extension only accepts 'pdf' or 'png'" << std::endl;
        exit(0);
    }
    
    std::string listfile = args["list"].as<std::string>();
    std::string ytitle = args["ytitle"].as<std::string>();
    std::string outputdir = args["outputdir"].as<std::string>();

    auto pullLogs = GetPullLogs(listfile);
    auto pulls = GetPulls(pullLogs);

    std::vector <double> y_values;
    for (const auto& pair : pulls){
        y_values.push_back(std::stod(std::to_string(pair.first)));
    }

    std::vector <double> values;
    std::vector <double> errors;
    std::string outputname;

    for (const auto& pair : pulls){
        values.push_back(pair.second.mu.mean);
        errors.push_back(pair.second.mu.mean_error);
    }
    DrawCompatibilityPlot(outputdir, "mu_Mean", ytitle, 0., 0.5, values, errors, y_values, extension, true);
    values.clear();
    errors.clear();

    for (const auto& pair : pulls){
        values.push_back(pair.second.mu.width);
        errors.push_back(pair.second.mu.width_error);
    }
    DrawCompatibilityPlot(outputdir, "mu_Width", ytitle, 1., 0.5, values, errors, y_values, extension);
    values.clear();
    errors.clear();

    for (const auto& pair : pulls){
        values.push_back(pair.second.sigma.mean);
        errors.push_back(pair.second.sigma.mean_error);
    }
    DrawCompatibilityPlot(outputdir, "sigma_Mean", ytitle, 0., 0.5, values, errors, y_values, extension);
    values.clear();
    errors.clear();

    for (const auto& pair : pulls){
        values.push_back(pair.second.sigma.width);
        errors.push_back(pair.second.sigma.width_error);
    }
    DrawCompatibilityPlot(outputdir, "sigma_Width", ytitle, 1., 0.5, values, errors, y_values, extension);
    values.clear();
    errors.clear();

    for (const auto& pair : pulls){
        values.push_back(pair.second.slope.mean);
        errors.push_back(pair.second.slope.mean_error);
    }
    DrawCompatibilityPlot(outputdir, "slope_Mean", ytitle, 0., 0.5, values, errors, y_values, extension);
    values.clear();
    errors.clear();

    for (const auto& pair : pulls){
        values.push_back(pair.second.slope.width);
        errors.push_back(pair.second.slope.width_error);
    }
    DrawCompatibilityPlot(outputdir, "slope_Width", ytitle, 1., 0.5, values, errors, y_values, extension);
    values.clear();
    errors.clear();

    for (const auto& pair : pulls){
        values.push_back(pair.second.nsig.mean);
        errors.push_back(pair.second.nsig.mean_error);
    }
    DrawCompatibilityPlot(outputdir, "nsig_Mean", ytitle, 0., 0.5, values, errors, y_values, extension);
    values.clear();
    errors.clear();

    for (const auto& pair : pulls){
        values.push_back(pair.second.nsig.width);
        errors.push_back(pair.second.nsig.width_error);
    }
    DrawCompatibilityPlot(outputdir, "nsig_Width", ytitle, 1., 0.5, values, errors, y_values, extension);
    values.clear();
    errors.clear();

    for (const auto& pair : pulls){
        values.push_back(pair.second.nbkg.mean);
        errors.push_back(pair.second.nbkg.mean_error);
    }
    DrawCompatibilityPlot(outputdir, "nbkg_Mean", ytitle, 0., 0.5, values, errors, y_values, extension);
    values.clear();
    errors.clear();

    for (const auto& pair : pulls){
        values.push_back(pair.second.nbkg.width);
        errors.push_back(pair.second.nbkg.width_error);
    }
    DrawCompatibilityPlot(outputdir, "nbkg_Width", ytitle, 1., 0.5, values, errors, y_values, extension);
    values.clear();
    errors.clear();

    return 0;
}

#endif
