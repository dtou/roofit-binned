#ifndef PLOTGENERATOR_CPP
#define PLOTGENERATOR_CPP

#include "TCanvas.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "Generator.h"
#include "Model.h"

int main(){

    RooRealVar mass("mass", "mass", 4900, 5900);
    Generator generator(mass, 0);
    generator.ConfigureSignal(1000000, 5366.79, 20, 1.5, 3, 2, 4);
    generator.ConfigureBackground(150000, -0.002);

    Model model(mass);
    model.ConfigureSignal(1000000, 5366.79, 20, 1.5, 3, 2, 4);
    model.ConfigureBackground(150000, -0.002);

    auto * pdf = model.GetPDF();
    auto dataset = generator.Generate();

    auto * frame = mass.frame();

    dataset->plotOn(frame);
    model.GetPDF()->plotOn(frame, RooFit::Components(RooArgSet(*model.GetSignal())), RooFit::LineColor(kRed));
    model.GetPDF()->plotOn(frame, RooFit::Components(RooArgSet(*model.GetBackground())), RooFit::FillStyle(3001), RooFit::FillColor(kBlue));

    TCanvas c("c", "c", 800, 600);
    frame->Draw();
    c.SaveAs("generator.pdf");
    c.SetLogy();
    frame->SetMinimum(1e-1);
    c.Update();
    c.SaveAs("generator_log.pdf");

    delete dataset;
    delete frame;

    return 0;
}

#endif
